! function(e) {
    function t(t) {
        var n = e(this),
            a = e.extend({}, e.fn.treegrid.defaults, t);
        return n.each(function() {
            var t = e(this);
            t.data("treegrid-settings", a), t.on("click", ">tbody>tr>td:first-child>.treegrid-container>.treegrid-expander", h).on("mousedown", ">tbody>tr>td:first-child>.treegrid-container", v), f(O.getRoots.call(t), 1)
        })
    }

    function n(t) {
        var n = t.closest("table").data("treegrid-settings");
        n.onExpand.call(t) && (t.addClass("expanded"), t.find(">td:first>.treegrid-container>.treegrid-expander").removeClass("treegrid-expander-collapsed").addClass("treegrid-expander-expanded"), o(t).not(t).each(function() {
            var t = e(this);
            p(t) ? t.hide() : t.show()
        }))
    }

    function a(e) {
        var t = e.closest("table").data("treegrid-settings");
        t.onCollapse.call(e) && (e.removeClass("expanded"), e.find(">td:first>.treegrid-container>.treegrid-expander").removeClass("treegrid-expander-expanded").addClass("treegrid-expander-collapsed"), o(e).not(e).hide())
    }

    function r(t, n) {
        var a, r = o(t).last().next(),
            d = [],
            i = s(t),
            l = t;
        "TABLE" !== l.prop("tagName") && (l = l.closest("table")), a = l.find(">thead>tr,>tbody>tr").first().find(">th,>td").length, e.each(n, function(t, n) {
            var o = e(n);
            if (!l.find(">tbody>tr.treegrid-" + s(o)).length) {
                null !== i && o.addClass("treegrid-parent-" + i);
                var c = o.find(">td").length;
                if (a > c)
                    for (var t = o.find(">td").length; a > t; t++) o.append("<td>");
                else c > a && o.find(">td").eq(a - 1).nextAll().remove();
                r.length ? o.insertBefore(r) : l.find(">tbody").append(o), d.push(o[0])
            }
        }), d = e(d), null === i && f(d), f(t, t.data("depth"));
        var c = t.closest("table").data("treegrid-settings");
        c.onAdd.call(t, d)
    }

    function d(e) { return e.parent().find(">.treegrid-" + l(e)) }

    function i(e) { return e.parent().find(">.treegrid-parent-" + s(e)) }

    function o(t) { var n = t; return "TR" !== t.prop("tagName") ? n.not(t) : (i(t).each(function() { n = n.add(o(e(this))) }), n) }

    function s(e) { var t = /treegrid-([A-Fa-f0-9_]+)/; return t.test(e.attr("class")) ? t.exec(e.attr("class"))[1] : null }

    function l(e) { var t = /treegrid-parent-([A-Fa-f0-9_]+)/; return t.test(e.attr("class")) ? t.exec(e.attr("class"))[1] : null }

    function c(e) { return e.data("count") && !e.hasClass("expanded") }

    function u(e) { return e.data("count") && e.hasClass("expanded") }

    function f(t, n, a) {
        void 0 === n && (n = 1), t.each(function() {
            var t = e(this).data("depth", n),
                r = i(t),
                d = r.length;
            void 0 === t.data("count") || t.data("loaded") || t.data("count") == d ? t.data("loadNeeded") || (t.data({ loaded: !0, count: d }), d && a && t.addClass("expanded")) : t.data("loadNeeded", !0), $td = t.find(">td:first"), $container = $td.find(">.treegrid-container"), 0 === $container.length && ($container = e('<div class="treegrid-container">').html($td.html()), $td.html("").append($container)), $container.find(".treegrid-expander").remove(), $expander = e('<span class="treegrid-expander">').prependTo($container), t.data("count") && (t.hasClass("expanded") ? $expander.addClass("treegrid-expander-expanded") : $expander.addClass("treegrid-expander-collapsed")), $container.css("marginLeft", n * $expander.width()), p(t) && t.hide(), f(r, n + 1, a)
        })
    }

    function h() {
        var t = e(this);
        (t.hasClass("treegrid-expander-expanded") || t.hasClass("treegrid-expander-collapsed")) && O.toggle.call(t.closest("tr"))
    }

    function p(e) { if (null === l(e)) return !1; var t = d(e); return c(t) ? !0 : p(t) }

    function g(t) { var n = t.closest("table").data("treegrid-settings"); return o(t).not(t).remove(), e.isFunction(n.source) && !t.hasClass("loading") && (t.addClass("loading"), n.source.call(t, s(t), function(e) { console.log("complete: " + e.length), t.removeData("loadNeeded").data("loaded", !0), r(t, e), t.removeClass("loading"), O.expand.call(t) })), "string" !== e.type(n.source) || t.hasClass("loading") || (t.addClass("loading"), e.post(n.source, { id: s(t) }, function(e) { t.removeDate("loadNeeded").data("loaded", !0), r(t, e), t.removeClass("loading") }, "json")), !1 }

    function v(t) {
        if (0 === t.button) {
            var n = e(this),
                a = n.closest("table").data("treegrid-settings");
            if (a.enableMove) { var r = e(t.target); if (!r.hasClass("treegrid-expander") && (a.moveHandle === !1 || n.find(a.moveHandle)[0] == r[0])) { S = n.closest("tr"), Y = t.pageX, N = t.pageY; var d = n.offset(); return X = d.left - t.pageX, j = d.top - t.pageY, e(document).on("mouseup", m).on("mousemove", x), !1 } }
        }
    }

    function m(t) { H && $(), e(document).off("mouseup", m).off("mousemove", x) }

    function x(e) {
        var t = Math.max(Math.abs(e.pageX - Y), Math.abs(e.pageY - N)),
            n = S.closest("table").data("treegrid-settings");
        t >= n.moveDistance && !H ? b(e) : H && C(e)
    }

    function b(t) {
        if (H = !0, A(), B = S.find(">td:first>.treegrid-container").clone().addClass("dragging").css({ left: t.pageX + X, top: t.pageY + j }), B.find(">.treegrid-expander").remove(), B.appendTo("body"), !e(".treegrid-move-indicator").length) {
            T = e('<div class="treegrid-move-indicator">').appendTo("body");
            var n = S.closest("table"),
                a = n.data("treegrid-settings");
            a.onMoveStart.call(n, S, B)
        }
    }

    function C(t) {
        B.css({ left: t.pageX + X, top: t.pageY + j });
        var n = null;
        e.each(D, function(e, a) { return t.pageX >= a[0] && t.pageY >= a[1] && t.pageX <= a[0] + a[2] && t.pageY <= a[1] + a[3] ? (y(t, a) && (n = a[4]), !1) : void 0 }), null !== F && n !== F && M(null == n), F = n
    }

    function $() {
        H = !1;
        var e = S.closest("table"),
            t = e.data("treegrid-settings");
        t.onMoveStop.call(e, S, B), B.remove(), T.remove(), R !== !1 && (window.clearTimeout(R), R = !1), null !== F && w()
    }

    function y(e, t) {
        var n, a, r = t[3] / 4,
            d = 3 * r,
            i = e.pageY - t[1];
        if (r > i ? (n = t[1], a = 0) : i >= d ? (n = t[1] + t[3], a = 2) : (n = t[1] + t[3] / 2, a = 1), t[4] == F && a == E) return !0;
        if (1 == a && null === s(t[4])) return !1;
        var o = t[4];
        c(o) && R === !1 && (R = window.setTimeout(function() { O.expand.call(o), R = !1 }, 500));
        var l = S.closest("table"),
            u = l.data("treegrid-settings");
        return u.onMoveOver.call(l, S, B, t[4], a) ? (T.css({ display: "block", left: t[4].find(">td:first>.treegrid-container").offset().left, top: n }), E = a, !0) : !1
    }

    function M(e) {
        var t = S.closest("table"),
            n = t.data("treegrid-settings");
        n.onMoveOut.call(t, S, B, F), e && T.hide(), R !== !1 && (window.clearTimeout(R), R = !1)
    }

    function w() {
        M(!0);
        var e = S.closest("table"),
            t = e.data("treegrid-settings"),
            n = t.onMove.call(e, S, F, E);
        n !== !1 && O.move.call(S, F, E)
    }

    function A() {
        var t = [];
        o(S).each(function() { t.push(s(e(this))) }), D = [], S.parent().find("tr").each(function() {
            var n = e(this);
            if (-1 === e.inArray(s(n), t)) {
                var a = n.offset();
                D.push([a.left, a.top, n.width(), n.height(), n])
            }
        })
    }
    var T, Y, N, X, j, D, E, O = {
            toggle: function() { return this.each(function() { $this = e(this), u($this) ? O.collapse.call($this) : c($this) && O.expand.call($this) }) },
            expand: function() {
                var t = !1;
                return this.each(function() {
                    var a = e(this);
                    c(a) && (a.data("loaded") || g(a)) && (n(a), t = !0)
                }), H && A(), this
            },
            collapse: function() {
                var t = !1;
                return this.each(function() {
                    var n = e(this);
                    u(n) && (a(n), t = !0)
                }), H && A(), this
            },
            add: function(t) { return this.each(function() { r(e(this), t) }), H && A(), this },
            remove: function() {
                return this.each(function() {
                    var t = e(this),
                        n = d(t);
                    o(t).remove(), f(n, n.data("depth"))
                }), H && A(), this
            },
            move: function(e, t) {
                var n, a, r = d(this);
                n = 0 == t ? e.prev() : o(e).last(), a = 1 === t ? s(e) : l(e), this.removeClass("treegrid-parent-" + l(this)), null !== a && this.addClass("treegrid-parent-" + a), $branch = o(this), n.length ? $branch.insertAfter(n) : $branch.prependTo(this.parent()), p(this) && $branch.hide(), r.length && f(r, r.data("depth"));
                var i = d(this);
                i.length ? f(i, i.data("depth")) : f(this)
            },
            getRoots: function() { var t = []; return this.find(">tbody>tr").each(function() { null === l(e(this)) && t.push(this) }), e(t) },
            getChildNodes: function() { var t = e(); return this.each(function() { t = t.add(i(e(this))) }), t },
            getBranch: function() { var t = e(); return this.each(function() { t = t.add(o(e(this))) }), t },
            getParent: function() { var t = e(); return this.each(function() { t = t.add(d(e(this))) }), t },
            isCollapsed: function() { var t = !1; return this.each(function() { return t = c(e(this)), t ? !1 : void 0 }), t },
            isExpanded: function() { var t = !1; return this.each(function() { return t = u(e(this)), t ? !1 : void 0 }), t }
        },
        S = null,
        B = null,
        F = null,
        H = !1,
        R = !1;
    e.fn.treegrid = function(n) { return O[n] ? O[n].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof n && n ? void e.error("Method with name " + n + " does not exists for jQuery.treegrid") : t.apply(this, arguments) }, e.fn.treegrid.defaults = { source: null, enableMove: !1, moveDistance: 10, moveHandle: !1, onExpand: function() { return !0 }, onCollapse: function() { return !0 }, onAdd: function() {}, onMoveStart: function() {}, onMoveStop: function() {}, onMoveOver: function() { return !0 }, onMoveOut: function() {}, onMove: function() { return !0 } }
}(jQuery);