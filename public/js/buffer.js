// Create our stage canvas.
var stage = document.createElement('canvas');
stage.style.border = '1px solid #000';
document.body.appendChild(stage);

// Create offscreen buffer for our text rendering.
// This way all we have to do is draw our buffer to 
// the main canvas rather than drawing text each frame.
var textBuffer = document.createElement('canvas');

// Lets cache a text rendering.
var text = 'google';
var fontSize = 24;
var fontFamily = 'Arial';
var m = measureText(text, fontSize, fontFamily);
// Resize the buffer.
textBuffer.width = m.width;
textBuffer.height = m.height;
// Render to our buffer.
var ctx = textBuffer.getContext('2d');
ctx.font = fontSize + 'px/' + fontSize + 'px ' + fontFamily;
ctx.fillText(text, 0, m.height);

// Now draw our buffer to the stage.
stage.getContext('2d').drawImage(textBuffer, 0, 0);