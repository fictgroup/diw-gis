<!DOCTYPE html>
<html lang="en">

<head>

  <!-- Basic Page Needs
  ================================================== -->
  <meta charset="utf-8">
  <title>GIS กรมโรงงานอุตสาหกรรม</title>

  <!-- Mobile Specific Metas
  ================================================== -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Bootstrap App Landing Template">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0">
  <meta name="author" content="Themefisher">
  <meta name="generator" content="Themefisher Small Apps Template v1.0">

  <!-- Favicon -->
  <!-- <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" /> -->

  <!-- PLUGINS CSS STYLE -->
  <link rel="stylesheet" href="../plugins/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="../plugins/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="../plugins/slick/slick.css">
  <link rel="stylesheet" href="../plugins/slick/slick-theme.css">
  <link rel="stylesheet" href="../plugins/fancybox/jquery.fancybox.min.css">
  <link rel="stylesheet" href="../plugins/aos/aos.css">

  <!-- CUSTOM CSS -->
  <link href="../css/style.css" rel="stylesheet">

</head>

<body class="body-wrapper" data-spy="scroll" data-target=".privacy-nav">


  <nav class="navbar main-nav navbar-expand-lg px-2 px-sm-0 py-2 py-lg-0">
    <div class="container">
      <a class="navbar-brand" href="index.php"><img src="../img/logo1.png" style="width: 64px;" alt="logo"></a><a class="navbar-brand" href="index.php"><img src="../img/logo text.png" style="width: 200px;" alt="logo"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="ti-menu"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item @@about">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item @@contact">
            <a class="nav-link" href="https://www.facebook.com/%E0%B9%80%E0%B8%82%E0%B8%95%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%81%E0%B8%AD%E0%B8%9A%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%AD%E0%B8%B8%E0%B8%95%E0%B8%AA%E0%B8%B2%E0%B8%AB%E0%B8%81%E0%B8%A3%E0%B8%A3%E0%B8%A1-%E0%B9%80%E0%B8%9E%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%9E%E0%B8%B1%E0%B8%92%E0%B8%99%E0%B8%B2%E0%B8%AD%E0%B8%A2%E0%B9%88%E0%B8%B2%E0%B8%87%E0%B8%A2%E0%B8%B1%E0%B9%88%E0%B8%87%E0%B8%A2%E0%B8%B7%E0%B8%99-102408165191023/?ref=pages_you_manage" target="_blank">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!--====================================
=            Hero Section            =
=====================================-->
  <section class="section gradient-banner">
    <div class="shapes-container">
      <div class="shape" data-aos="fade-down-left" data-aos-duration="1500" data-aos-delay="100"></div>
      <div class="shape" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="100"></div>
      <div class="shape" data-aos="fade-up-right" data-aos-duration="1000" data-aos-delay="200"></div>
      <div class="shape" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200"></div>
      <div class="shape" data-aos="fade-down-left" data-aos-duration="1000" data-aos-delay="100"></div>
      <div class="shape" data-aos="fade-down-left" data-aos-duration="1000" data-aos-delay="100"></div>
      <div class="shape" data-aos="zoom-in" data-aos-duration="1000" data-aos-delay="300"></div>
      <div class="shape" data-aos="fade-down-right" data-aos-duration="500" data-aos-delay="200"></div>
      <div class="shape" data-aos="fade-down-right" data-aos-duration="500" data-aos-delay="100"></div>
      <div class="shape" data-aos="zoom-out" data-aos-duration="2000" data-aos-delay="500"></div>
      <div class="shape" data-aos="fade-up-right" data-aos-duration="500" data-aos-delay="200"></div>
      <div class="shape" data-aos="fade-down-left" data-aos-duration="500" data-aos-delay="100"></div>
      <div class="shape" data-aos="fade-up" data-aos-duration="500" data-aos-delay="0"></div>
      <div class="shape" data-aos="fade-down" data-aos-duration="500" data-aos-delay="0"></div>
      <div class="shape" data-aos="fade-up-right" data-aos-duration="500" data-aos-delay="100"></div>
      <div class="shape" data-aos="fade-down-left" data-aos-duration="500" data-aos-delay="0"></div>
    </div>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-6 order-2 order-md-1 text-center text-md-left">
          <h1 class="text-white font-weight-bold mb-4">GIS APPLICATION</h1>
          <p class="text-white mb-5">โครงการวิเคราะห์ศักยภาพพื้นที่เขตประกอบการอุตสาหกรรมเพื่อการพํฒนาที่ยั่งยืนด้วยระบบ GIS</p>
          <a href="http://ptizapp.diw.go.th/login" target="_blank" class="btn btn-main-md">Sign In</a>
        </div>
        <div class="col-md-6 text-center order-1 order-md-2">
          <img class="img-fluid" src="../img/W4F3.png" alt="screenshot">
        </div>
      </div>
    </div>
  </section>
  <!--====  End of Hero Section  ====-->

  <section class="section pt-0 position-relative pull-top">
    <div class="container">
      <div class="rounded shadow p-5 bg-white">
        <div class="row">
          <div class="col-lg-4 col-md-6 mt-5 mt-md-0 text-center">
            <i class="ti-location-pin text-primary h1"></i>
            <h3 class="mt-4 text-capitalize h5 ">ADDRESS</h3>
            <p class="regular text-muted">75/6 ถ.พระรามที่ 6 แขวงทุ่งพญาไท เขตราชเทวี กรุงเทพฯ 10400</p>
          </div>
          <div class="col-lg-4 col-md-6 mt-5 mt-md-0 text-center">
            <i class="ti-mobile text-primary h1"></i>
            <h3 class="mt-4 text-capitalize h5 ">PHONE</h3>
            <p class="regular text-muted">โทรศัพท์ : 02-2024162</p>
            <p class="regular text-muted">โทรสาร : 02-2024183</p>
          </div>
          <div class="col-lg-4 col-md-12 mt-5 mt-lg-0 text-center">
            <i class="ti-world text-primary h1"></i>
            <h3 class="mt-4 text-capitalize h5 ">WEBSITE</h3>
            <p class="regular text-muted"><a href="https://www.diw.go.th" target="_blank">www.diw.go.th</a></p>
          </div>
        </div>
      </div>
    </div>
  </section>



  <!--============================
=            Footer            =
=============================-->
  <footer>
    <div class="text-center bg-dark py-4">
      <small class="text-secondary">Copyright &copy; <script>
          document.write(new Date().getFullYear())
        </script>. Designed &amp; Developed by <a href="http://www.fictgroup.com/" target="_blank">FICT Associate</a></small class="text-secondary">
    </div>


  </footer>


  <!-- To Top -->
  <div class="scroll-top-to">
    <i class="ti-angle-up"></i>
  </div>

  <!-- JAVASCRIPTS -->
  <script src="../plugins/jquery/jquery.min.js"></script>
  <script src="../plugins/bootstrap/bootstrap.min.js"></script>
  <script src="../plugins/slick/slick.min.js"></script>
  <script src="../plugins/fancybox/jquery.fancybox.min.js"></script>
  <script src="../plugins/syotimer/jquery.syotimer.min.js"></script>
  <script src="../plugins/aos/aos.js"></script>
  <!-- google map -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAgeuuDfRlweIs7D6uo4wdIHVvJ0LonQ6g"></script>
  <script src="../plugins/google-map/gmap.js"></script>

  <script src="../js/script.js"></script>
</body>

</html>