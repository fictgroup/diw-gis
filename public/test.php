<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no" />
    <title>Load a basic WebMap | Sample | ArcGIS API for JavaScript 4.19</title>

    <style>
        html,
        body,
        #viewDiv {
            padding: 0;
            margin: 0;
            height: 100%;
            width: 100%;
        }
    </style>

    <link rel="stylesheet" href="https://js.arcgis.com/4.19/esri/themes/light/main.css" />

    <script src="https://js.arcgis.com/4.19/"></script>

    <script>
        require(["esri/config", "esri/views/MapView", "esri/WebMap", "esri/widgets/LayerList", "esri/widgets/Search"], function(esriConfig, MapView, WebMap, LayerList, Search) {
            /************************************************************
             * Creates a new WebMap instance. A WebMap must reference
             * a PortalItem ID that represents a WebMap saved to
             * arcgis.com or an on-premise portal.
             *
             * To load a WebMap from an on-premise portal, set the portal
             * url with esriConfig.portalUrl.
             ************************************************************/

            esriConfig.portalUrl = "https://arcgisapp.diw.go.th/portal";

            var webmap = new WebMap({
                portalItem: {
                    // autocasts as new PortalItem()
                    id: "36e94d4042a54df99b50e79c7658ccf6"
                }
            });

            /************************************************************
             * Set the WebMap instance to the map property in a MapView.
             ************************************************************/
            var view = new MapView({
                map: webmap,
                container: "viewDiv"
            });

            view.when(function() {

                var searchWidget = new Search({
                    view: view
                });

                // Add the search widget to the top right corner of the view
                view.ui.add(searchWidget, {
                    position: "top-right"
                });

                var layerList = new LayerList({
                    view: view
                });

                // Add widget to the top right corner of the view
                view.ui.add(layerList, "top-right");


            });
        });
    </script>
</head>

<body>
    <div id="viewDiv"></div>
</body>

</html>