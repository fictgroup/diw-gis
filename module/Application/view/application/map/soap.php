<?php

$wsdl = "http://oaep.diw.go.th:8080/diwFacTest/Fac?wsdl";

$client = new SoapClient($wsdl, array(
    "trace" => 1,
    "exceptions" => 1
));

echo '<pre>';
// var_dump($client->__getFunctions());
// var_dump($client->__getTypes());
echo '</pre>';

// $parameter = array('arg0' => array('FID' => '10104900125392'));


$parameter = array('arg0' => array('FID' => '3-42(1)-2/18สป'));

try {
    // $response = $client->__soapCall("diwfacFID", $parameter);
    $response = $client->diwfacFID($parameter);
    echo '<pre>';
    print_r($response->return->FAC->FID);
    echo '</pre>';
} catch (SoapFault $e) {
    echo $e->getMessage();
}


