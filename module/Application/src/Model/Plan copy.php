<?php

namespace Application\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;

use Zend\Paginator\Paginator;

class Plan
{
    private $factory;
    private $adapter;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
        $this->adapter = $factory['adapter'];
    }

    public function get($id = null)
    {

        $db = new Sql($this->adapter['read']);

        $select = $db->select()
            ->from('user_group')
            ->order(['user_group_id' => 'ASC']);

        if (!is_null($id)) {
            $select->where(['user_group_id' => $id]);
        } else {
            $select->where(['user_group_active' => 1]);
        }

        // echo $db->buildSqlString($select); exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        if (!is_null($id)) {
            return $results->current();
        } else {
            return $results;
        }
    }

    public function getActivityList()
    {
        $db = new Sql($this->adapter['read']);

        $select = $db->select()
            ->from('plan_activity');

        // echo $db->buildSqlString($select); exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        return $results;
    }

    public function getPlanByCode($code)
    {
        $db = new Sql($this->adapter['read']);

        $select = $db->select()
            ->from('plan')
            ->join('plan_activity', 'plan.plan_activity_id = plan_activity.plan_activity_id', ['plan_activity_name'], 'LEFT')
            ->where(['code' => $code]);

        // echo $db->buildSqlString($select); exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        return $results;
    }

    public function save($data, $id = null)
    {

        $table = new TableGateway('plan', $this->adapter['write']);

        if (is_null($id)) {
            //insert
            $table->insert($data);
        } else {
            //update
            $table->update($data, ['plan_id' => $id]);
        }
    }

    public function delete($id)
    {
        $table = new TableGateway('plan', $this->adapter['write']);

        $table->delete(['plan_id' => $id]);
    }
}
