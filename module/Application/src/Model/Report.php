<?php

namespace Application\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;


class Report
{
    private $factory;
    private $adapter;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
        $this->adapter = $factory['adapter'];
    }

    public function getFactoryType($code)
    {
        $db = new Sql($this->adapter['read']);
        $select = $db->select()
            ->from('disp_class')
            ->where(['disp_code' => $code]);

        // echo $db->buildSqlString($select);
        // exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();
        return $results->current();
    }

    public function getreport()
    {

        $db = new Sql($this->adapter['read']);

        $select = $db->select()
            ->from('dashboard_report');

        // echo $db->buildSqlString($select); exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        return $results;
    }

    public function getsearch()
    {

        $db = new Sql($this->adapter['read']);

        $select = $db->select()
            ->from('factory_search');

        // echo $db->buildSqlString($select); exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        return $results;
    }

    public function save($data, $id = null)
    {
        $table = new TableGateway('dashboard_report', $this->adapter['write']);

        if (is_null($id)) {
            //insert
            $table->insert($data);
        } else {
            //update
            $table->update($data, ['report_id' => $id]);
        }
    }

    public function saveSearch($data, $id = null)
    {
        $table = new TableGateway('factory_search', $this->adapter['write']);

        if (is_null($id)) {
            //insert
            $table->insert($data);
        } else {
            //update
            $table->update($data, ['search_id' => $id]);
        }
    }

    public function delete($id)
    {

        $table = new TableGateway('dashboard_report', $this->adapter['write']);
        $table->delete(['report_id' => $id]);

        // $this->save(['user_active' => 0], ['user_id' => $id]);
    }

    public function deleteSearch($id)
    {

        $table = new TableGateway('factory_search', $this->adapter['write']);
        $table->delete(['search_id' => $id]);

        // $this->save(['user_active' => 0], ['user_id' => $id]);
    }
}
