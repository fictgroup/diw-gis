<?php

namespace Application\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;

use Zend\Paginator\Paginator;


use Utils\Utils;

class Map
{
    private $factory;
    private $adapter;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
        $this->adapter = $factory['adapter'];
    }

    public function paginator($search)
    {

        $db = new Sql($this->adapter['read']);

        $select = $db->Select()
            ->from('user')
            ->join('jobs', 'user.job_id = jobs.job_id', ['job_name'], 'LEFT')
            ->where("(user_code LIKE '%" . $search . "%' OR user_firstname LIKE '%" . $search . "%')")
            ->where(['user_active' => 1, 'user_external' => 0])
            ->where('user_id != 2');

        // echo $db->buildSqlString($select); exit();

        $resultSetPrototype = new ResultSet();

        $paginatorAdapter = new DbSelect(
            $select,
            $this->adapter['read'],
            $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }

    public function get($id = null)
    {

        $db = new Sql($this->adapter['read']);

        $select = $db->select()
            ->from('user')
            ->join('company', 'user.company_id = company.company_id', ['company_name'], 'LEFT')
            ->join(['company_at' => 'company'], 'user.company_at_id = company_at.company_id', ['company_at_name' => 'company_name'], 'LEFT')
            ->join('department', 'user.department_id = department.department_id', ['department_name'], 'LEFT')
            ->join(['department_at' => 'department'], 'user.department_at_id = department_at.department_id', ['department_at_name' => 'department_name'], 'LEFT')
            ->join('jobs', 'user.job_id = jobs.job_id', ['job_name'], 'LEFT')
            ->where('user_active = 1')
            ->order(['user_firstname' => 'ASC']);

        if (!is_null($id)) {
            $select->where(['user_id' => $id]);
        } else {
            $select->where('user_id != 2');
        }

        // echo $db->buildSqlString($select); exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        if (!is_null($id)) {
            return $results->current();
        } else {
            return $results;
        }
    }

    public function getSearch()
    {

        $db = new Sql($this->adapter['read']);

        $select = $db->select()
            ->from('factory_search');

        // echo $db->buildSqlString($select); exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        return $results;
    }

    public function advanceSearch($data)
    {

        $db = new Sql($this->adapter['read']);

        $select = $db->select()
            ->columns(array("factory_code"))
            ->from('factory')
            ->join('factory_boiler', 'factory.factory_id = factory_boiler.factory_id', [], 'LEFT')
            ->join('factory_hazard_waste', 'factory.factory_id = factory_hazard_waste.factory_id', [], 'LEFT')
            ->join('factory_hazard_waste_non', 'factory.factory_id = factory_hazard_waste_non.factory_id', [], 'LEFT')
            ->join('factory_machine', 'factory.factory_id = factory_machine.factory_id', [], 'LEFT')
            ->join('factory_material', 'factory.factory_id = factory_material.factory_id', [], 'LEFT')
            ->join('factory_product', 'factory.factory_id = factory_product.factory_id', [], 'LEFT');

        if (!empty($data['boiler'])) {
            $select->where("boiler_pressure LIKE '%" . $data['boiler'] . "%'");
        }

        if (!empty($data['hazard_waste'])) {
            $select->where("hazard_name LIKE '%" . $data['hazard_waste'] . "%'");
        }
        if (!empty($data['non_hazard_waste'])) {
            $select->where("non_hazard_name LIKE '%" . $data['non_hazard_waste'] . "%'");
        }
        if (!empty($data['machine'])) {
            $select->where("machine_hp LIKE '%" . $data['machine'] . "%'");
        }
        if (!empty($data['material'])) {
            $select->where("material_name LIKE '%" . $data['material'] . "%'");
        }
        if (!empty($data['product'])) {
            $select->where("product_name LIKE '%" . $data['product'] . "%'");
        }

        // ->group('factory_code');


        // echo $db->buildSqlString($select);
        // exit();


        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        return $results;
    }



    public function save($data, $where = null)
    {

        $table = new TableGateway('user', $this->adapter['write']);

        if (is_null($where)) {
            //insert
            $table->insert($data);
        } else {
            //update
            $table->update($data, $where);
        }
    }

    public function delete($id)
    {

        $this->save(['user_active' => 0], ['user_id' => $id]);
    }
}
