<?php

namespace Application\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;

use Zend\Paginator\Paginator;

class Group
{
    private $factory;
    private $adapter;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
        $this->adapter = $factory['adapter'];
    }

    public function get($id = null)
    {

        $db = new Sql($this->adapter['read']);

        $select = $db->select()
            ->from('user_group')
            ->order(['user_group_id' => 'ASC']);

        if (!is_null($id)) {
            $select->where(['user_group_id' => $id]);
        } else {
            $select->where(['user_group_active' => 1]);
        }

        // echo $db->buildSqlString($select); exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        if (!is_null($id)) {
            return $results->current();
        } else {
            return $results;
        }
    }

    public function paginator($search)
    {

        $db = new Sql($this->adapter['read']);

        $select = $db->Select()
            ->from('user_group')
            ->where("(user_group_name LIKE '%" . $search . "%')")
            ->where(['user_group_active' => 1])
            ->order(['user_group_name']);

        // echo $db->buildSqlString($select); exit();

        $resultSetPrototype = new ResultSet();

        $paginatorAdapter = new DbSelect(
            $select,
            $this->adapter['read'],
            $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }

    public function save($data, $id = null)
    {

        $table = new TableGateway('user_group', $this->adapter['write']);

        if (is_null($id)) {
            //insert
            $table->insert($data);
        } else {
            //update
            $table->update($data, ['user_group_id' => $id]);
        }
    }

    public function delete($id)
    {

        $this->save(['user_group_active' => 0], ['user_group_id' => $id]);
    }
}
