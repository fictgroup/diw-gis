<?php

namespace Application\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;

use Zend\Paginator\Paginator;
use Application\Model\User;

class Log
{
    private $factory;
    private $adapter;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
        $this->adapter = $factory['adapter'];
    }


    public function paginator($search)
    {

        $db = new Sql($this->adapter['read']);

        $select = $db->Select()
            ->from('logs')
            ->where("(log_message LIKE '%" . $search . "%' OR log_date LIKE '%" . $search . "%')")
            ->order('log_date DESC');

        // echo $db->buildSqlString($select); exit();

        $resultSetPrototype = new ResultSet();

        $paginatorAdapter = new DbSelect(
            $select,
            $this->adapter['read'],
            $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }

    public function save($userId)
    {
        $table = new TableGateway('logs', $this->adapter['write']);

        $mUser = new User($this->factory);
        $userInfo = $mUser->get($userId);

        $data = [
            'log_type' => 'login',
            'log_message' => $userInfo['user_login'] . ' ' . $userInfo['user_name'] . ' ทำการเข้าสู่ระบบ',
            'log_date' => date('Y-m-d H:i:s'),
            'user_id' => $userId
        ];

        $table->insert($data);
    }
}
