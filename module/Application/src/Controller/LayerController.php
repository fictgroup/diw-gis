<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use Zend\Mvc\Controller\Plugin\Forward;
use Zend\View\Renderer\PhpRenderer;

use Application\Model\Layer;
use Application\Model\Group;

class LayerController extends AbstractActionController
{
    private $factory;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
    }

    public function indexAction()
    {

        $search = $this->params()->fromQuery('search', '');

        $groupId = $_SESSION['login']['user_group_id'];

        $mGroup = new Group($this->factory);
        $groupInfo = $mGroup->get($groupId);

        $permission = json_decode($groupInfo['user_group_permission']);

        $mLayer = new Layer($this->factory);

        $layerList = $mLayer->get($permission->layers, 'feature', $search);


        return new ViewModel(array(
            'layerList' => $layerList,
            'search' => $search,
        ));

        // $mLayer = new Layer($this->factory);
        // $objectId = $mLayer->getLayerObjectIdByGroup($groupId);
        // return new ViewModel(array(
        //     'objectId' => $objectId,
        // ));
    }

    public function listAction()
    {

        $layerId = $this->params()->fromQuery('layer_id');

        $mLayer = new Layer($this->factory);
        $layerInfo = $mLayer->get($layerId);

        // print_r($layerInfo);

        $groupId = $_SESSION['login']['user_group_id'];
        $editLyerInfo = $mLayer->getEditLayerInfo($layerId, $groupId);

        $primaryKey = 'OBJECTID';
        $layerKey = 'OBJECTID';
        $displayName = '';

        if (isset($editLyerInfo['edit_layer_key'])) {
            $primaryKey = $editLyerInfo['edit_layer_primary_key'];
            $layerKey = json_decode($editLyerInfo['edit_layer_key']);
            $displayName = $editLyerInfo['edit_layer_display_field'];
        }

        return new ViewModel(array(
            'layerInfo' => $layerInfo,
            'primaryKey' => $primaryKey,
            'layerKey' => $layerKey,
            'displayName' => $displayName,
        ));
    }

    public function editAction()
    {
        $this->layout()->setTemplate('layout/none');

        $id = $this->params()->fromPost('id');
        $layerId = $this->params()->fromPost('layer_id');

        $mLayer = new Layer($this->factory);
        $layerInfo = $mLayer->get($layerId);

        $groupId = $_SESSION['login']['user_group_id'];
        $editLyerInfo = $mLayer->getEditLayerInfo($layerId, $groupId);

        $primaryKey = $editLyerInfo['edit_layer_primary_key'];

        return new ViewModel(array(
            'layerInfo' => $layerInfo,
            'primaryKey' => $primaryKey,
            'id' => $id,
        ));
    }

    public function saveAction()
    {

        $objectid = $_POST['objectid'];
        $data = $_POST['data'];


        $attr = [
            'objectid' => (int)$objectid,
        ];

        foreach ($data as $key => $value) {

            if ($value == 'image') {
                //upload
                if ($_FILES[$key]['error'] == 0) {
                    $name = $this->upload($_FILES[$key]);
                    $attr[$key] = $name;
                }
            } else {
                $attr[$key] = $value;
            }
        }

        $data = [(object) ['attributes' => $attr]];

        echo json_encode($data, JSON_UNESCAPED_UNICODE);

        exit();
    }

    private function upload($file)
    {
        $tmp = explode('.', $file['name']);
        $ext = array_pop($tmp);
        $name = uniqid() . '.' . $ext;
        $destination = PUBLIC_PATH . '/uploads/' . $name;
        move_uploaded_file($file['tmp_name'], $destination);
        return $name;
    }

    public function onDispatch(MvcEvent $e)
    {

        if (!isset($_SESSION['login'])) {
            return $this->redirect()->toRoute('login', ['action' => 'index']);
        }

        $this->layout()->menu = 'layer';

        $response = parent::onDispatch($e);

        return $response;
    }
}
