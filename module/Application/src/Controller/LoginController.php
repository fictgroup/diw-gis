<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use Zend\Mvc\Controller\Plugin\Forward;
use Zend\View\Renderer\PhpRenderer;

use Application\Model\User;

use Utils\Utils;

class LoginController extends AbstractActionController
{
    private $factory;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
    }

    public function indexAction()
    {
        $this->layout()->setTemplate('layout/authen');

        if (isset($_SESSION['login'])) {

            return $this->redirect()->toRoute('map', ['action' => 'index']);
        }

        // $user = $this->forward()->dispatch('user', ['action' => 'detail', 'from' => 'dashboard']);

        $login = $this->params()->fromPost('login', false);
        $password = $this->params()->fromPost('password', false);

        if ($login && $password) {

            $mUser = new User($this->factory);

            $isLogin = $mUser->login($login, $password);
            if ($isLogin) {
                return $this->redirect()->toRoute('map', ['action' => 'index']);
            } else {
                return $this->redirect()->toRoute('login', ['action' => 'index'], ['params' => ['error' => 'xxxx']]);
            }
        }
    }

    public function logoutAction()
    {
        unset($_SESSION['login']);
        return $this->redirect()->toRoute('login', ['action' => 'index']);
    }


    public function onDispatch(MvcEvent $e)
    {

        $this->layout()->menu = 'home';

        $response = parent::onDispatch($e);

        return $response;
    }
}
