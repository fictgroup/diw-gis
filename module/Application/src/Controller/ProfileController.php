<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use Zend\Mvc\Controller\Plugin\Forward;
use Zend\View\Renderer\PhpRenderer;

use Application\Model\User;

class ProfileController extends AbstractActionController
{
    private $factory;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
    }

    public function indexAction()
    {

        $mUser = new User($this->factory);
        $profile = $mUser->get($_SESSION['login']['user_id']);


        return new ViewModel(array(
            'profile' => $profile,
        ));
    }

    public function onDispatch(MvcEvent $e)
    {

        if (!isset($_SESSION['login'])) {
            return $this->redirect()->toRoute('login', ['action' => 'index']);
        }

        $this->layout()->menu = 'profile';

        $response = parent::onDispatch($e);

        return $response;
    }
}
