<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use Zend\Mvc\Controller\Plugin\Forward;
use Zend\View\Renderer\PhpRenderer;

use Application\Model\Report;

use Utils\Utils;

class DashboardController extends AbstractActionController
{
    private $factory;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
    }

    public function indexAction()
    {

        $mReport = new Report($this->factory);
        $dispClass = $mReport->getreport();

        return new ViewModel(array(
            'dispClass' => $dispClass,
        ));
    }

    public function getfactypeAction()
    {

        $this->layout()->setTemplate('layout/none');
        $data = $this->params()->fromPost('code');

        $code = explode(',', $data);
        unset($code[0]);

        $mReport = new Report($this->factory);

        // print_r($code);
        // exit();

        $html = '<div class="body table-responsive"><table width="100%" class="table">';
        $html .= '<tr><th>ลำดับ</th><th>ประเภทหรือชนิดของโรงงาน</th></tr>';
        foreach ($code as $value) {

            $row = $mReport->getFactoryType($value);

            $html .= '<tr><td>' . $row['disp_code'] . '</td><td>' . $row['disp_type'] . '</td></tr>';
        }

        $html .= "</table></div>";

        echo $html;
        exit();
    }

    public function onDispatch(MvcEvent $e)
    {

        if (!isset($_SESSION['login'])) {
            return $this->redirect()->toRoute('login', ['action' => 'index']);
        }

        $this->layout()->menu = 'dashboard';

        $response = parent::onDispatch($e);

        return $response;
    }
}
