<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

 

namespace Application;

use Application\Model\Config;
use Application\Model\Evaluate;
use Application\Model\Permission;

use Zend\Db\Adapter\Adapter;

use Zend\Mvc\MvcEvent;

use Utils\Utils;

class Module
{
    const VERSION = '3.0.3-dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function onBootstrap(MvcEvent $event)
    {

        session_start();

        define("TOKEN", "1234"); // uniqid

        $application = $event->getApplication();
        $config      = $application->getConfig();
        $viewModel = $event->getViewModel();
    
        $readAdapter = new Adapter($config['db']['adapters']['ReadOnlyAdapter']);
        $writeAdapter = new Adapter($config['db']['adapters']['WriteAdapter']);


        $factory = [
            'adapter' => [
                'read' => $readAdapter,
                'write' => $writeAdapter,
            ],
            'translator' => null,
        ];

        $utils = new Utils($factory);
        
        $uid = $utils->getUserId();

        if($uid){

            // $mPermission = new Permission($factory);
            // $viewModel->setVariable('permission', $mPermission->getUserPermission($uid));


        }

    }

}
