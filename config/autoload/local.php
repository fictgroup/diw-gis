<?php

/**
 * Local Configuration Override
 *
 * This configuration override file is for overriding environment-specific and
 * security-sensitive configuration information. Copy this file without the
 * .dist extension at the end and populate values as needed.
 *
 * @NOTE: This file is ignored from Git by default with the .gitignore included
 * in ZendSkeletonApplication. This is a good practice, as it prevents sensitive
 * credentials from accidentally being committed into version control.
 */

return [
    'db' => [
        'adapters' => [
            'WriteAdapter' => [
                'driver' => 'Pdo_Mysql',
                'hostname' => '127.0.0.1',
                'database' => 'diw_gis',
                'username' => 'root',
                'password' => '1q2w3e4r!',
                'port' => '3306',
                'charset' => 'utf8'
            ],
            'ReadOnlyAdapter' => [
                'driver' => 'Pdo_Mysql',
                'hostname' => '127.0.0.1',
                'database' => 'diw_gis',
                'username' => 'root',
                'password' => '1q2w3e4r!',
                'port' => '3306',
                'charset' => 'utf8'
            ],
        ],
    ],
    'email' => [
        'host'        => 'smtp.pepipost.com',
        'port'        => 587,
        'connection_class'  => 'login',
        'connection_config' => [
            'username' => '',
            'password' => '',
            'ssl'      => 'tls',
        ],
    ],
    'translator' => [
        'locale' => (isset($_COOKIE['lang']) ? $_COOKIE['lang'] : 'th'),
        'fallbackLocale' => 'th',
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => getcwd() .  '/data/language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
];
