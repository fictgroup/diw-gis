<?php

/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */


return [
    'db' => [
        'adapters' => [
            'WriteAdapter' => [
                'driver' => 'Pdo_Mysql',
                'hostname' => '127.0.0.1',
                'database' => 'railway_gis',
                'username' => 'railway',
                'password' => 'Windows@95',
                'port' => '3306',
                'charset' => 'utf8'
            ],
            'ReadOnlyAdapter' => [
                'driver' => 'Pdo_Mysql',
                'hostname' => '127.0.0.1',
                'database' => 'railway_gis',
                'username' => 'railway',
                'password' => 'Windows@95',
                'port' => '3306',
                'charset' => 'utf8'
            ],
        ],
    ],
    'email' => [
        'host'        => 'smtp.pepipost.com',
        'port'        => 587,
        'connection_class'  => 'login',
        'connection_config' => [
            'username' => '',
            'password' => '',
            'ssl'      => 'tls',
        ],
    ],
    'translator' => [
        'locale' => (isset($_COOKIE['lang']) ? $_COOKIE['lang'] : 'th'),
        'fallbackLocale' => 'th',
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => getcwd() .  '/data/language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
];
